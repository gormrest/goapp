package eapp

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/gormrest/goethe"
)

func RegisterAppGroup(gp *gin.RouterGroup) {
	// endpoint: /App/test
	gp.GET("/test", func(ctx2 *gin.Context) {
		GC := ctx2.MustGet("JWT").(*goethe.GClaims)
		ctx2.JSON(200, GC.NewResponse(
			[]string{"message"},
			map[string]interface{}{
				"message": GC,
			},
			http.StatusOK,
		))
	})
}
