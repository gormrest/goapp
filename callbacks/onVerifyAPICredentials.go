package callbacks

import (
	"log"
	"time"

	"gitlab.com/gormrest/goapp/model"
	"gitlab.com/gormrest/goapp/service"
	"gitlab.com/gormrest/goethe"
	"golang.org/x/crypto/bcrypt"
)

const (
	MIN_NAME_LEN = 5
	MIN_KEY_LEN  = 8
)

var nameCache map[string]model.Credentials

// VerifyService verifies the service name and key.
//
// Parameters:
//
//	name: Service name as a byte array.
//	key: Service key as a byte array.
//
// Returns:
//
//	Authentication status as an integer.
func OnVerifyAPICredentials(name, key []byte) goethe.GOETHE_AUTH_LEVEL {
	if len(name) < MIN_NAME_LEN {
		log.Printf("Name or key too short: %d, %d", len(name), len(key))
		return goethe.AUTH_NONE
	}

	DB, err := service.GetDB()

	if err != nil {
		log.Println("DB not connected")
		return goethe.AUTH_NONE
	}

	var credentials model.Credentials
	// check if name is in cache
	credentials, isInCache := nameCache[string(name)]

	if credentials.Name == "" {
		// not found in cache
		if isInCache {
			// nil credentials in cache means that the name doesnt exist
			log.Println("Name doesnt exist")
			return goethe.AUTH_NONE
		} else {
			// find service by name
			DB.First(&credentials, "name = ? AND (valid_from IS NULL OR valid_from <= NOW()) AND (NOT valid_until OR valid_until > NOW())", name)

			if credentials.Name == "" {
				return goethe.AUTH_NONE
			}
		}
	} else {
		// check if credentials has expired
		if credentials.ValidUntil.Before(time.Now()) {
			// remove from cache
			delete(nameCache, string(name))
			return goethe.AUTH_NONE
		}
	}

	now := time.Now()

	// update last access
	credentials.LastAccess = &now

	// save last access
	DB.Save(&credentials)

	if len(key) < MIN_KEY_LEN {
		log.Println("Key too short")
		return goethe.AUTH_SERVICE_NAME
	}

	// compare service key with hashed value in database
	err = bcrypt.CompareHashAndPassword([]byte(credentials.Key), key)
	if err != nil {
		log.Println("Service key does not match")
		return goethe.AUTH_SERVICE_WRONG_KEY
	}

	return goethe.AUTH_SERVICE_KEY
}
