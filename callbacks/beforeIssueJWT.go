package callbacks

import (
	"errors"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/gormrest/goapp/utils"
)

func BeforeIssueJWT(c *gin.Context, claims *jwt.MapClaims) error {
	if c == nil || claims == nil {
		return errors.New("claims or context is nil")
	}

	// create session with database and control string
	controlString := utils.CreateControlHash(c.ClientIP(), c.GetHeader("User-Agent"))

	// set IP and browser in claims
	(*claims)["cs"] = controlString

	return nil
}
