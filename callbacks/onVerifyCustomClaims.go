package callbacks

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/gormrest/goapp/model"
	"gitlab.com/gormrest/goapp/utils"
	"gitlab.com/gormrest/goethe"
)

func OnVerifyCustomClaims(c *gin.Context, claims *jwt.MapClaims) error {
	// need to verify claims from token
	if c == nil || claims == nil {
		return errors.New("claims or context is nil")
	}

	// check the expiration
	if claims.Valid() != nil {
		return errors.New("token expired")
	}

	controlString := utils.CreateControlHash(c.ClientIP(), c.GetHeader("User-Agent"))

	if (*claims)["cs"] != controlString {
		c.AbortWithStatus(http.StatusUnauthorized)
		return errors.New("invalid control string")
	}

	user, exist := (*claims)["user"].(uint)

	if exist {
		_, err := model.KeepAlive(uint(user), c.ClientIP(), c.GetHeader("User-Agent"))

		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, goethe.ResponseData{
				Action: []string{"message", "logout"},
				Data: map[string]interface{}{
					"message":       "Your session has expired. Please login again.",
					"logout_reason": model.LogoutReasonSessionExpired,
				},
			})
			return errors.New("invalid session")
		}
	}

	// save claims for later use
	return nil
}
