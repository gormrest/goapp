package main

import (
	"log"
	"os"
	"time"

	"gitlab.com/gormrest/goapp/callbacks"
	"gitlab.com/gormrest/goapp/endpoints/eapp"
	"gitlab.com/gormrest/goapp/endpoints/euser"
	"gitlab.com/gormrest/goapp/model"
	"gitlab.com/gormrest/goapp/service"

	"github.com/getsentry/sentry-go"
	sentrygin "github.com/getsentry/sentry-go/gin"
	"gitlab.com/gormrest/goethe"
)

const ACCESS_FILE = "./data/.access"

func init() {
	// init sentry before anything else
	if sentryDSN := os.Getenv("SENTRY_DSN"); sentryDSN != "" {
		if err := sentry.Init(sentry.ClientOptions{
			Dsn:              os.Getenv("SENTRY_DSN"),
			EnableTracing:    true,
			TracesSampleRate: 1.0,
		}); err != nil {
			log.Printf("Sentry initialization failed: %v\n", err)
		}
	} else {
		log.Println("!!! Sentry is not active !!!")
	}

	DB := service.MustGetDB()

	DB.AutoMigrate(model.Credentials{}, model.User{}, model.UserLogin{}, model.UserSession{})
	DB.AutoMigrate(model.UserAccountAction{})

	// creates new credentials for acces if file credentials does not exist
	if _, err := os.Stat(ACCESS_FILE); err != nil {
		// credentials file not exists
		c, e := model.CreateCredentials()

		if e != nil {
			log.Fatal("failed to create credentials: ", e)
		}

		// create file and add credentials
		f, err := os.Create(ACCESS_FILE)
		if err != nil {
			log.Fatal("failed to write credentials file: ", err)
		}

		f.Write([]byte(time.Now().String() + "\n"))
		f.Write([]byte(c.Name + "\n"))
		f.Write([]byte(c.Key + "\n"))

		f.Close()
	}
}

func main() {
	// create gin engine
	goethe.Callbacks = &goethe.GCallbacks{
		// issueJWT with custom claims
		BeforeIssueJWT: callbacks.BeforeIssueJWT,
		// verify Service Name and Key
		OnVerifyCredentials: callbacks.OnVerifyAPICredentials,
		// verify custom JWT claims (e.g. API session)
		OnVerifyClaims: callbacks.OnVerifyCustomClaims,
	}

	engine := goethe.GoetheEntry()

	// attach gin-sentry middleware
	engine.Use(sentrygin.New(sentrygin.Options{}))

	// example: protected group requires at least service name
	appGroup := engine.Group("/App", goethe.RequireAuthLevel(goethe.AUTH_USER_LOGGED_IN))
	{
		eapp.RegisterAppGroup(appGroup)
	}

	userGroup := engine.Group("/User", goethe.RequireAuthLevel(goethe.AUTH_SERVICE_KEY))
	{
		euser.RegisterUserGroup(userGroup)
	}

	engine.Run() // listen and serve on 8080
}
