package utils

import (
	"crypto/sha256"
	"encoding/hex"
)

// Generate a SHA-256 hash from the given string and return hex string
func GenerateHexHash(input string) string {
	// Convert the input string to bytes
	inputBytes := []byte(input)

	// Create a new SHA-256 hash object
	hash := sha256.New()

	// Write the data to the hash
	hash.Write(inputBytes)

	// Get the hash sum as a byte slice
	hashSum := hash.Sum(nil)

	// Convert the byte slice to a hex string
	hexHash := hex.EncodeToString(hashSum)

	return hexHash
}

// Create control string for validating user session JWT
//
// @param IP - IP address of client
// @param userAgent - User-Agent header of client
//
// @return control string
func CreateControlHash(IP, userAgent string) string {
	return GenerateHexHash(IP + userAgent)
}
