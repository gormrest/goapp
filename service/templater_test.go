package service_test

import (
	"testing"

	s "gitlab.com/gormrest/goapp/service"
)

func TestTemplater(t *testing.T) {
	tpl := s.NewTemplater()

	cnt, err := tpl.PreloadDir("../templates/")

	if err != nil {
		t.Error(err)
	}

	if cnt == 0 {
		t.Error("No templates preloaded")
	}

	content, err := tpl.RenderToString("cs-cz/register_confirm.html", map[string]string{
		"abcd": "defg",
	})

	if err != nil {
		t.Error(err)
	}

	if content == "" {
		t.Error("Rendered template is empty")
	}
}

func TestTemplaterRenderToString(t *testing.T) {
	tpl := s.NewTemplater()

	cnt, err := tpl.PreloadDir("../templates/")

	if err != nil {
		t.Error(err)
	}

	if cnt == 0 {
		t.Error("No templates preloaded")
	}

	rendered, err := tpl.RenderToString("cs-cz/register_confirm.html", map[string]string{
		"world": "world",
	})

	if err != nil {
		t.Error(err)
	}

	if rendered == "" {
		t.Error("Rendered template is empty")
	}
}
