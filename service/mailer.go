package service

import (
	"log"
	"os"
	"strconv"
	"time"

	mail "github.com/xhit/go-simple-mail/v2"
)

var SMTPmailer *mail.SMTPClient

// Get SMTP mailer, if not connected, get credentials from environment variables and create a new connection
//
// Expects to have the following environment variables set: SMTP_HOST, [SMTP_PORT = 587], SMTP_USER and SMTP_PASSWORD
//
// Returns a pointer to a SMTPClient
func GetMailer() *mail.SMTPClient {
	if SMTPmailer == nil {
		// if mailer already exists, return it
		return SMTPmailer
	}

	SMTPort, err := strconv.Atoi(os.Getenv("SMTP_PORT"))

	server := mail.NewSMTPClient()

	if err != nil {
		// should happen if SMTP_PORT is not set, so we set default value
		server.Port = 587
	} else {
		server.Port = SMTPort
	}

	// SMTP Server
	server.Host = os.Getenv("SMTP_HOST")
	server.Username = os.Getenv("SMTP_USER")
	server.Password = os.Getenv("SMTP_PASSWORD")
	server.Encryption = mail.EncryptionSTARTTLS

	if server.Host == "" || server.Username == "" || server.Password == "" {
		log.Fatal("SMTP_HOST, SMTP_USER and SMTP_PASSWORD must be set in your .env file")
	}

	// Variable to keep alive connection
	server.KeepAlive = false

	// Timeout for connect to SMTP Server
	server.ConnectTimeout = 10 * time.Second

	// Timeout for send the data and wait respond
	server.SendTimeout = 10 * time.Second

	// SMTP client
	SMTPmailer, err := server.Connect()

	if err != nil {
		log.Fatal(err)
	}

	return SMTPmailer
}

// Create message for sending
//
// @param content - content of message
// @param from - sender email address (must be the same as SMTP_USER)
// @param to - slice of recepients' email addresses
func CreateMessage(content, from string, to []string, html bool) (*mail.Email, error) {
	email := mail.NewMSG()

	email.SetFrom(from)

	// add all recepients to the message as Blind Carbon Copy to hide them from each other
	email.AddBcc(to...)

	if html {
		email.SetBody(mail.TextHTML, content)
	} else {
		email.SetBody(mail.TextPlain, content)
	}

	return email, nil
}
