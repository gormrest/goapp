package service

import (
	"bytes"
	"fmt"
	"html/template"
	"path/filepath"
)

type Templater struct {
	// templates is a map of templates
	Templates map[string]*template.Template
}

var Tpl *Templater

// NewTemplater creates new templater instance and preloads all templates from directory
// Templater allows to precompile templates and store them in memory for faster execution
//
// @return Templater
func NewTemplater() *Templater {
	if Tpl != nil {
		return Tpl
	}

	Tpl = &Templater{
		Templates: make(map[string]*template.Template),
	}

	return Tpl
}

// Precompile template file
//
// @param filePath string
// @return error If template file not found or template parsing failed
func (t *Templater) Preload(filePath string) error {
	// parse template and add it into templates map
	tpl, err := template.ParseFiles(filePath)

	if err != nil {
		return err
	}

	if tpl == nil {
		return fmt.Errorf("template %s not found", filePath)
	}

	t.Templates[filePath] = tpl

	return nil
}

// Precompile all templates from directory
//
// @param path string
// @return error If dir not found or template parsing failed returns the original error
func (t *Templater) PreloadDir(path string) (uint8, error) {
	// Glob preload all templates from directory
	templateFiles, err := filepath.Glob(path + "/**/*.html")

	if err != nil {
		return 0, err
	}

	var cnt uint8 = 0

	for _, file := range templateFiles {
		err := t.Preload(file)

		if err != nil {
			return cnt, err
		}

		dir, filename := filepath.Split(file)
		// dirname like cs-cz
		dir = filepath.Base(dir)
		// now concat it
		filename = dir + "/" + filename

		tpl, err := template.ParseFiles(file)

		if err != nil {
			return cnt, err
		}

		// process template
		t.Templates[filename] = tpl

		cnt++
	}

	return cnt, nil
}

// RenderToString renders template to string
//
// @param template string relatively to templates dir (eg "cs-cz/register_confirm")
// @param vars map[string]string variables to pass to template
// @return string Rendered template or empty string if template not found
func (t *Templater) RenderToString(template string, vars map[string]string) (string, error) {
	tpl, exist := t.Templates[template]

	if !exist {
		return "", fmt.Errorf("template %s not found", template)
	}

	var buffer bytes.Buffer
	err := tpl.Execute(&buffer, vars)

	if err != nil {
		return "", err
	}

	return buffer.String(), nil
}
