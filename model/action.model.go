package model

import (
	"encoding/json"
	"time"

	"gitlab.com/gormrest/goapp/service"
)

// Account action valid time in seconds
const ActionDefaultExpiry = 1800

// Actions that may be invoked by Hash (email verification, password reset, etc.)
type UserAccountAction struct {
	ActionID     uint       `gorm:"primarykey;unique;not null;"` // ID for invoking action (email verification, password reset, etc.)
	UserID       User       `gorm:"foreignKey:UserID;"`          // user ID
	CreatedAt    *time.Time `gorm:"type:timestamp; default:current_timestamp; index:idx_action_valid_date"`
	ValidUntil   *time.Time `gorm:"type:timestamp; default:null; index:idx_action_valid_date"`
	LastExecuted *time.Time `gorm:"type:timestamp;on update current_timestamp; default:null"`
	ExecLimit    *uint8     `gorm:"type:TINYINT;default:1"` // number of times this action can be executed, default to 1, NULL means unlimited
	ActionData   string     `gorm:"type:json"`
}

// Creates new Account action with UUID etc. Process data into JSON
func CreateAccountAction(User User, ActionData map[string]interface{}, count uint8) (*UserAccountAction, error) {
	validUntil := time.Now().Add(time.Second * ActionDefaultExpiry)
	actionData, err := json.Marshal(ActionData)

	if err != nil {
		return nil, err
	}

	action := UserAccountAction{
		UserID:     User,
		ActionData: string(actionData),
		ValidUntil: &validUntil,
		ExecLimit:  &count,
	}

	DB := service.MustGetDB()
	DB.Save(&action)

	return &action, nil
}
