package model

import (
	"errors"
	"fmt"
	"time"

	"gitlab.com/gormrest/goapp/service"
	"golang.org/x/crypto/bcrypt"
)

// Enums

type LogoutReason uint8

const (
	LogoutReasonUserLogout LogoutReason = iota
	LogoutReasonSessionExpired
	LogoutReasonSessionInvalidated
)

type UserState string

const (
	UserUnconfirmed   UserState = "UNCONFIRMED"
	UserStateActive   UserState = "ACTIVE"
	UserStateInactive UserState = "INACTIVE"
	UserStateDeleted  UserState = "DELETED"
)

// Define a custom type for the ENUM
type ExtLoginMethodEnum string

// Define constants for the allowed values of the ENUM
const (
	// 'login', 'google', 'facebook', 'github', 'twitter', 'linkedin'
	ExtLoginPassword ExtLoginMethodEnum = "PASSWORD"
	ExtLoginGoogle   ExtLoginMethodEnum = "GOOGLE"
)

// Session expire time in seconds (3600 = 1 hour)
const SessionExpireTime = 3600

// Check if login method is allowed
func IsLoginMethodAllowed(loginMethod ExtLoginMethodEnum) bool {
	// check if login method is allowed
	return loginMethod == ExtLoginPassword || loginMethod == ExtLoginGoogle
}

// User meta data
type UserMeta struct {
	UserID uint   `gorm:"foreignKey:UserID;primaryKey"`
	Key    string `gorm:"type:varchar(64);primaryKey"`
	Val    string
}

// User session for identifying current logged user
type UserSession struct {
	SessionID    uint               `gorm:"primarykey"`
	UserID       uint               `gorm:"foreignKey:UserID;unique_index:idx_type_user"`
	Type         ExtLoginMethodEnum `gorm:"type:ENUM('PASSWORD','GOOGLE');unique_index:idx_type_user;not null"`
	IP           string             `gorm:"type:varchar(64)"`
	UserAgent    string             `gorm:"type:varchar(256)"`
	LogoutReason LogoutReason       `gorm:"type:ENUM('USER_LOGOUT', 'SESSION_EXPIRED', 'SESSION_INVALIDATED');default:null"`
	CreatedAt    time.Time          `gorm:"type:timestamp;default:current_timestamp"`
	LastSeen     time.Time          `gorm:"type:timestamp;on update current_timestamp; default:null"`
	ValidUntil   time.Time          `gorm:"type:timestamp;default:null"`
}

// User login name and secret pair for login via different services
type UserLogin struct {
	UserID uint               `gorm:"foreignKey:UserID;unique_index:idx_type_user"`
	Type   ExtLoginMethodEnum `gorm:"type:ENUM('PASSWORD', 'GOOGLE');unique_index:idx_type_user;not null"`
	Name   string             `gorm:"type:varchar(64);index:service_name;unique_index:idx_type_name"`
	Secret string             `gorm:"type:varchar(196)"`
}

/** User model */
type User struct {
	UserID   uint           `gorm:"primarykey"`
	Status   UserState      `gorm:"type:ENUM('UNCONFIRMED', 'ACTIVE', 'DELETED');default:'UNCONFIRMED'"`
	Metadata *[]UserMeta    `gorm:"foreignKey:UserID"`
	Login    []UserLogin    `gorm:"foreignKey:UserID"`
	Sessions *[]UserSession `gorm:"foreignKey:UserID"`
	Email    string         `gorm:"unique"`
	Name     string
	Sname    string
}

func (u *User) String() string {
	var res string
	res += fmt.Sprintf("ID: %d\n", u.UserID)
	res += fmt.Sprintf("State: %s\n", u.Status)
	res += fmt.Sprintf("Email: %s\n", u.Email)
	res += fmt.Sprintf("Name: %s\n", u.Name)
	res += fmt.Sprintf("Sname: %s\n", u.Sname)
	res += fmt.Sprintf("Login: %v\n", u.Login)
	res += fmt.Sprintf("Sessions: %v\n", u.Sessions)
	res += fmt.Sprintf("UserMeta: %v\n", u.Metadata)

	return res
}

// Create a new user, hash password and create login
//
// @param u - User pointer
//
// @return error - wrong credentials, bcrypt fails, database write fail
func CreateUser(u *User) error {
	if u.Email == "" || u.Name == "" || u.Sname == "" {
		return errors.New("email and name required")
	}
	if u.Login == nil || len(u.Login) != 1 {
		// need at least password login
		return errors.New("no login credentials provided")
	}

	DB := service.MustGetDB()

	login := u.Login[0]

	// hash password with salty string
	hash, err := bcrypt.GenerateFromPassword([]byte(login.Secret), bcrypt.DefaultCost)
	if err != nil {
		return errors.New("failed to hash secret")
	}

	// insert hashed password
	u.Login[0].Secret = string(hash)
	// finally create user
	err = DB.Create(u).Error
	if err != nil {
		return err
	}

	return nil
}

// Login user via login string and secret
//
// @param loginType - type of login method
// @param name - login string
// @param secret - secret string
// @param IP - IP address of client
// @param userAgent - User-Agent header of client
//
// @return User session
func Login(loginType ExtLoginMethodEnum, name, secret, IP, userAgent string) (*UserSession, error) {
	DB := service.MustGetDB()

	var login UserLogin
	err := DB.Where("name = ? AND type = ?", name, loginType).First(&login).Error
	if err != nil {
		return nil, errors.New("login not found")
	}

	err = bcrypt.CompareHashAndPassword([]byte(login.Secret), []byte(secret))
	if err != nil {
		return nil, errors.New("wrong secret")
	}

	var user User
	err = DB.Where("user_id = ?", login.UserID).First(&user).Error

	if err != nil {
		return nil, err
	}

	// create session
	session := UserSession{
		Type:       loginType,
		IP:         IP,
		UserAgent:  userAgent,
		UserID:     user.UserID,
		CreatedAt:  time.Now(),
		LastSeen:   time.Now(),
		ValidUntil: time.Now().Add(time.Second * SessionExpireTime),
	}

	err = DB.Create(&session).Error
	if err != nil {
		fmt.Println("failed to create session: ", err)
		return nil, errors.New("failed to create session")
	}

	return &session, nil
}

// Check and update user session
//
// @param sessionID - ID of session
// @param IP - IP address of client
// @param userAgent - User-Agent header of client
//
// @return session - updated session
func KeepAlive(sessionID uint, IP, userAgent string) (*UserSession, error) {
	DB := service.MustGetDB()

	var session UserSession
	err := DB.Where("session_id = ?", sessionID).First(&session).Error
	if err != nil {
		return nil, errors.New("session not found")
	}

	if session.IP != IP || session.UserAgent != userAgent {
		return &session, errors.New("different IP or UserAgent")
	}

	// update timestamp in DB
	session.LastSeen = time.Now()
	session.ValidUntil = time.Now().Add(time.Second * SessionExpireTime)

	err = DB.Save(&session).Error

	if err != nil {
		return &session, errors.New("failed to update session")
	}

	return &session, nil
}

// Logout user session
//
// @param sessionID - ID of session
// @param reason - reason of logout
//
// @return error - session not found, failed to update session
func Logout(sessionID uint, reason LogoutReason) error {
	DB := service.MustGetDB()

	var session UserSession
	err := DB.Where("session_id = ?", sessionID).First(&session).Error
	if err != nil {
		return errors.New("session not found")
	}

	// update timestamp in DB
	session.ValidUntil = time.Now()
	session.LogoutReason = reason

	err = DB.Save(&session).Error

	if err != nil {
		return errors.New("failed to update session")
	}

	return nil
}
